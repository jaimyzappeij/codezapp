module.exports = {
  purge: [
    './resources/views/**/*.blade.php',
    './resources/css/**/*.css',
    './safelist.txt',
  ],
  theme: {
    colors: {
      black: {
        off: '#12120f',
        DEFAULT: '#000'
      },
      white: {
        DEFAULT: '#fff', 
      },
      brand: {
        yellow: '#FCBA02',
        error: '#a82323', 
        success: '#3b8f51', 
      },
      browser: {
        light: '#36363A',
        dark: '#1F2123',
        border: '#636465'
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/ui'),
    require('tailwind-safelist-generator')({
      patterns: [
        'text-{colors}',
        'border-{borderWidth}',
        'm-{margin}',
        'mt-{margin}',
        'my-{margin}',
        'mx-{margin}',
        'mb-{margin}',
        'mr-{padding}',
        'ml-{padding}',
        '{screens}:mt-{margin}',
        'p-{padding}',
        'pt-{padding}',
        'py-{padding}',
        'pl-{padding}',
        'pr-{padding}',
        'px-{padding}',
        'pb-{padding}',
        '{screens}:w-{width}'
      ],
    })
  ]
}
