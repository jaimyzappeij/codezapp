<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormEntry extends Model
{
    use HasFactory;

    /**
     * The name of the table for this model
     * @var string
     */
    protected $table = 'formentries';

    protected $fillable = ['form_id', 'data'];


    public function form()
    {
        return $this->belongsTo(Form::class);
    }

    public function getFieldsAttribute()
    {
        return json_decode($this->data, true); 
    }
}
