<?php

namespace App\Helpers;

class Templater
{
    /**
     * Render text as if it is a blade template
     * @param  string $string String that needs to be rendered as a blade template
     * @param  array  $data   Data that is used in the blade template string
     * @return string         HTML of the rendered blade string
     */
    public static function render($string, $data = [])
    {
        $php = \Blade::compileString($string);

        $obLevel = ob_get_level();
        ob_start();
        extract($data, EXTR_SKIP);

        try {
            eval('?' . '>' . $php);
        } catch (Exception $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }
            throw $e;
        } catch (Throwable $e) {
            while (ob_get_level() > $obLevel) {
                ob_end_clean();
            }
            throw new FatalThrowableError($e);
        }

        return ob_get_clean();
    }
}
