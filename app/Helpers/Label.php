<?php

namespace App\Helpers;

use App\TextLabel;

class Label
{
    /**
     * Get text labels from database
     * @param  string $key  Key of the text label
     * @return string       Text of the label or empty string if not found
     */
    public static function get(string $key)
    {
        $label = TextLabel::where('key', $key)->first();
        if ($label !== null) {
            return $label->text;
        }
        return '';
    }
}
