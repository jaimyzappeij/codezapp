<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrowserPage extends Model
{
    use HasFactory;

    /**
     * The name of the table for this model
     * @var string
     */
    protected $table = 'browserpages';
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $fillable = ['title', 'slug', 'content', 'opened'];
}
