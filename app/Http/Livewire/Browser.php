<?php

namespace App\Http\Livewire;

use App\BrowserPage;
use Livewire\Component;

class Browser extends Component
{
    public $tabs = [];
    public $all_pages = [];
    public $active = 'contact';

    /**
     * Array or listeners the component uses
     * @var array
     */
    protected $listeners = ['newBrowserTab'];

    /**
     * Function that is triggerd by the newBrowserTab listener
     * the function will create a new tab in the browser component if the tab is not aleady open
     * TODO: remove select query and replace with search in array for existing pages
     * @param  string $slug Slug of the browser page to open
     */
    public function newBrowserTab($slug)
    {
        $do_query = true;
        foreach ($this->tabs as $tab) {
            if ($tab->slug == $slug) {
                $do_query = false;
            }
        }
        $page = null;
        if ($do_query) {
            $page = BrowserPage::where('slug', $slug)->first();
            if ($page !== null) {
                $this->tabs[] = $page;
            }
        }
        if ($page !== null || !$do_query) {
            $this->active = $slug;
        } else {
            $this->active = '404';
        }
    }

    /**
     * Update active page/tab in the browser
     * @param string $page Slug of the new active page/tab
     */
    public function setActivePage($page)
    {
        $this->active = $page;
    }

    /**
     * When the component is mounted run query to fetch the nesseccery data
     */
    public function mount()
    {
        $this->tabs = BrowserPage::where('opened', true)->get();
        $this->pages = BrowserPage::all();
    }

    /**
     * Render bowser component with needed data
     * @return view Livewire broser component
     */
    public function render()
    {
        return view('livewire.home.browser')->with([
            'tabs' => $this->tabs,
            'pages' => $this->pages,
            'active' => $this->active
        ]);
    }
}
