<?php

namespace App\Http\Livewire\Games;

use Livewire\Component;

class Snake extends Component
{
    /**
     * Render snake game component
     * NOTE: Component should be embeded in a wire:igore because it uses a generated canvas
     * @return view livewire component
     */
    public function render()
    {
        return view('livewire.games.snake');
    }
}
