<?php

namespace App\Http\Livewire;

use App\FormEntry;
use App\Form as DBForm;
use Livewire\Component;

class Form extends Component
{
	/**
	 * The form that needs to be loaded in the component
	 * @var Array
	 */
	public $form; 

	/**
	 * The fields that need to be renedered in the form
	 * @var Array
	 */
	public $fields; 

	/**
	 * If the form is submitted 
	 * @var Bool
	 */
	public $submitted = false;

	/**
	 * Validation rules for the form. Will be created in the setupForm function 
	 * @var Array
	 */
	public $rules = [];

	/**
	 * Runs on mount and sets up all necessery data 
	 */
	public function mount($slug){
		$this->form = DBForm::where('slug', $slug)->first();
		if($this->form !== null){
			$this->fields = json_decode($this->form->fields, true); 
			$this->setupForm();
		}
	}

	/**
	 * Creates all the validation rules based on the form fields
	 */
	public function setupForm(){
		foreach($this->fields as $field){
			$this->{$field['name']} = '';
			if($field['required']){
				$this->rules[$field['name']] = 'required|';
			}
			switch ($field['type']) {
				case 'email':
				case 'phone':
				case 'number':
					$this->rules[$field['name']] .= $field['type'].'|';
					break;
			}
			$this->rules[$field['name']] = substr($this->rules[$field['name']] , 0, -1);
		}

	}

	/**
	 * Runs when fields are updated with data in the frontend
	 * Gets called by livewire
	 */
 	public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

	/**
	 * Submits form when data is validated
	 * Gets called by livewire
	 */
    public function submitForm()
    {
        $validatedData = $this->validate();
        $this->submitted = true;

        FormEntry::create([
        	'data' => json_encode($validatedData),
        	'form_id' => $this->form->id
        ]);

    }

    /**
     * Render the component with data 
     * @return view The component 
     */
    public function render()
    {
        return view('livewire.form')->with([
        	'form' => $this->form,
        	'fields' => $this->fields,
        	'submitted' => $this->submitted
        ]);
    }
}
