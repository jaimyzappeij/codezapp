<?php

namespace App\Http\Livewire;

use App\StackLogo;
use Livewire\Component;

class Stacklogos extends Component
{
    public $logos = [];

    /**
     * Trigger new tab in browser with the slug of the image to open infomation tab in the browser
     */
    public function addNewTab($slug)
    {
        $this->emit('newBrowserTab', $slug);
    }

    /**
     * When the component is mounted run query to fetch the nesseccery data
     */
    public function mount()
    {
        $this->logos = StackLogo::all();
    }

    /**
     * Render the stack logo component
     * @return view Livewire stack logos component
     */
    public function render()
    {
        return view('livewire.home.stacklogos')->with('logos', $this->logos);
    }
}
