<?php

namespace App\Http\Livewire\Home;

use Livewire\Component;

class ContactButton extends Component
{
    /**
     * Trigger newBroserTab in the browser component
     * NOTE: always open contact in browser (needs to be dynamic for easy repurposing)
     * TODO: fix issue when the browser component is not on the page.
     * TODO: change component to a regular button component with dynamic triggers and/or urls
     * @return [type] [description]
     */
    public function openContactPage()
    {
        $this->emit('newBrowserTab', 'contact');
    }

    /**
     * Render contact button
     * @return view livewire home contact button url
     */
    public function render()
    {
        return view('livewire.home.contact-button');
    }
}
