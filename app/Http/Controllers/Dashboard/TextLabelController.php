<?php

namespace App\Http\Controllers\Dashboard;

use App\TextLabel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TextLabelController extends Controller
{
    /**
     * Dashboard page of the text labels
     * @return view Dashboard page
     */
    public function index()
    {
        $tl = TextLabel::all();
        return view('dashboard.labels.index')->with('labels', $tl);
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        TextLabel::create($data);
        return redirect()->back();
    }

    /**
     * Update a text label in the database
     * @param  Request $request Request data
     * @return Redirect         Redirect back
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');

        $textLabel = TextLabel::find($id);
        $textLabel->key = $data['key'];
        $textLabel->text = $data['text'];
        $textLabel->location = $data['location'];
        $textLabel->save();

        return redirect()->back();
    }
}
