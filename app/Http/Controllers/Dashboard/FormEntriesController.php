<?php

namespace App\Http\Controllers\Dashboard;

use App\FormEntry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormEntriesController extends Controller
{
	public function index(){
		$entries = FormEntry::all();
		return view('dashboard.formentries.index')->with('entries', $entries);
	}
}
