<?php

namespace App\Http\Controllers\Dashboard;

use App\Form;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FormController extends Controller
{
    /**
     * Show the form admin page with forms overview
     * @return view The overview page
     */
    public function index()
    {
        $forms = Form::all();
        return view('dashboard.forms.index')->with([
            'forms' => $forms
        ]);
    }

    /**
     * Store a new form in the database
     * @return Redirect back
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        Form::create($data);
        return redirect()->back();
    }

    /**
     * Show edit page of form where fields can be added
     * @return view The edit page
     */
    public function edit($id)
    {
        $form = Form::find($id);
        $fields = ($form->fields !== null ? json_decode($form->fields) : []);
        return view('dashboard.forms.edit')->with([
            'form' => $form,
            'fields' => $fields
        ]);
    }

    /**
     * Update the fields of the form or change the form data
     * @return redirect back
     */
    public function update(Request $request, $id)
    {
        $form = Form::find($id);
        $data = $request->except('_token');

        if (isset($data['action']) && $data['action'] == 'add') {
            // WHEN THERE IS NO DATA SAVED YET
            $fields = [];
            if ($form->fields !== null && $form->fields !== '[]') {
                $fields = (array) json_decode($form->fields, true);
            }
            $data['required'] = (isset($data['required']) ? 1 : 0);
            $fields[$data['name']] = [
                'name' => $data['name'],
                'type' => $data['type'],
                'label' => $data['label'],
                'placeholder' => $data['placeholder'],
                'required' => $data['required']
            ];
            $fields = json_encode($fields);
            $fields = (empty($fields) ? null : $fields);

            $form->fields = $fields;
            $form->save();
        } else {
            // WHEN THERE ALREADY IS DATA SAVED
            $fields = [];
            foreach ($data as $key => $value) {
                @list($field, $name) = explode('---', $key);
                if (!isset($fields[$name])) {
                    $fields[$name] = [];
                }
                $fields[$name][$field] = $value;
            }

            foreach ($fields as &$field) {
                $field['required'] = (isset($field['required']) ? 1 : 0);
            }

            $form->fields = $fields;
            $form->save();
        }

        return redirect()->back();
    }
}
