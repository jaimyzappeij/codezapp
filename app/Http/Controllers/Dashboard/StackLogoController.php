<?php

namespace App\Http\Controllers\Dashboard;

use App\StackLogo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StackLogoController extends Controller
{
    /**
     * Show dashboard page for stack logo management
     * @return view dashboard page
     */
    public function index()
    {
        $sl = StackLogo::all();
        return view('dashboard.stacklogos.index')->with(['stacklogos' => $sl]);
    }

    /**
     * Store stack logo data in the database
     * @param  Request $request Request data
     * @return Redirect         Redirect back
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        StackLogo::create($data);
        return redirect()->back();
    }

    /**
     * Update a stack logo in the database
     * @param  Request $request Request data
     * @return Redirect         Redirect back
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');

        $logo = StackLogo::find($id);
        $logo->image = $data['image'];
        $logo->slug = $data['slug'];
        $logo->title = $data['title'];
        $logo->class =  $data['class'];
        $logo->save();

        return redirect()->back();
    }
}
