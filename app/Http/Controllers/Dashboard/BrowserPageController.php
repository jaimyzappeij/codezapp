<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\BrowserPage;
use Illuminate\Http\Request;

class BrowserPageController extends Controller
{
    /**
     * Dashboard page of the Browser pages
     * @return view Dashboard page
     */
    public function index()
    {
        $pages = BrowserPage::all();
        return view('dashboard.browser.index')->with('pages', $pages);
    }

    /**
     * Store browser page data and redirect back
     * @param  Request $request Request data
     * @return Redirect         Redirect back
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        if (isset($data['opened'])) {
            $data['opened'] = true;
        } else {
            $data['opened'] = false;
        }
        BrowserPage::create($data);
        return redirect()->back();
    }

    /**
     * update browser page data and redirect back
     * @param  Request $request Request data
     * @return Redirect         Redirect back
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        if (isset($data['opened'])) {
            $data['opened'] = true;
        } else {
            $data['opened'] = false;
        }
        $page = BrowserPage::find($id);

        $page->title = $data['title'];
        $page->slug = $data['slug'];
        $page->content = $data['content'];
        $page->opened = $data['opened'];
        $page->save();

        return redirect()->back();
    }
}
