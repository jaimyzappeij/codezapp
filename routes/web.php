<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::middleware('auth')->prefix('/dashboard')->group(function () {
    Route::get('/', 'Dashboard\DashboardController@index')->name('home');

    Route::get('/home/labels', 'Dashboard\TextLabelController@index');
    Route::post('/home/labels', 'Dashboard\TextLabelController@store');
    Route::post('/home/labels/{id}', 'Dashboard\TextLabelController@update');


    Route::get('/home/stacklogos', 'Dashboard\StackLogoController@index');
    Route::post('/home/stacklogos', 'Dashboard\StackLogoController@store');
    Route::post('/home/stacklogos/{id}', 'Dashboard\StackLogoController@update');

    Route::get('/home/browserpages', 'Dashboard\BrowserPageController@index');
    Route::post('/home/browserpages', 'Dashboard\BrowserPageController@store');
    Route::post('/home/browserpages/{id}', 'Dashboard\BrowserPageController@update');

    Route::get('/forms', 'Dashboard\FormController@index');
    Route::post('/forms', 'Dashboard\FormController@store');
    Route::any('/forms/{id}', 'Dashboard\FormController@edit');
    Route::post('/forms/update/{id}', 'Dashboard\FormController@update');

    Route::get('/form/entries', 'Dashboard\FormEntriesController@index');
});

// TEMP ROUTES
// TODO: Move functions to controller and create permanent routes

Route::get('/cv', function () {
    return view('cv');
});

Route::get('/cv-generate', function () {
    $html = view('cv')->render();
    $pdf = PDF::loadHTML($html)->setPaper('a4')->save(public_path().'/cv.pdf');
});
