# CodeZapp

The CodeZapp website runs on the tall-stack. This stack was used to keep development as simple as possible. The stack contains the following frameworks 

**Tailwind.css**

This framework contains almost all the css utility classes you need to create a design without ussing custom css. To keep the CSS file size to a minimun the CSS gets created based on the used classes and a safelist so only the necessary classes are loaded in the site. 

**Alpine.js**

Alpine.js is a tiny JavaScript framework that allows the creation of simple interactive components. This framework pairs perfectly with Livewire which is created by the same creator.

**Laravel**

A PHP framework with everything you need to build a website. 

**Livewire** 

Laravel view components, delivered seamlessly to users via JavaScript. This allows for a quick implementation of new elements and features into a website without the need t o write loads of custom javascript.

#### Note: This site is still under development