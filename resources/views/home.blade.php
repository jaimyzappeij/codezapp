@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10"> 
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Welcome back, {{ \Auth::user()->name }}!</h1>
                <p class="text-white">
                   In this dashboard you can change your front page settings, view your current projects and manage all files.
                </p>
                <div class="flex gap-4 mt-8 flex-start">
                    <div class="w-4/12 bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4">Homepage settings</div>
                        <div class="p-4 text-white">
                            <ul class="list-disc list-inside leading-6">
                                <li><a href="{{ '/dashboard/home/labels' }}" class=" underline">Edit texts</a></li>
                                <li><a href="{{ '/dashboard/home/stacklogos' }}" class=" underline">Stack logos</a></li>
                                <li><a href="{{ '/dashboard/home/browserpages' }}" class=" underline">Browser pages</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="w-4/12 bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4">Froms</div>
                        <div class="p-4 text-white">
                            <ul class="list-disc list-inside leading-6">
                                <li><a href="{{ '/dashboard/forms' }}" class=" underline">Forms</a></li>
                                <li><a href="{{ '/dashboard/form/entries' }}" class=" underline">Form entries</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection
