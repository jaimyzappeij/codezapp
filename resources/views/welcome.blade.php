@extends('layouts.app')

@section('content')

<main class="container mx-auto flex items-center px-6 mt-16 mb-32">
    <div class="flex flex-wrap flex-row">
        <div class="w-full md:w-8/12 pr-4">
            <h1 class="text-4xl md:text-7xl text-white mt-0 sm:mt-8 md:mt-20 mb-6">{!! Label::get('header_title') !!}</h1>    
            <p class="intro leading-6">{!! Label::get('header_intro') !!}</p>
            <div class="mt-8"></div>
            @livewire('home.contact-button')
        </div>
        <div class="w-full md:w-4/12">
            @livewire('stacklogos')
        </div>
    </div>  
</main>

<div class="mx-auto w-11/12 sm:w-10/12 md:w-8/12">
    @livewire('browser')
</div>  

<div class="pb-24 block"></div>

@endsection
