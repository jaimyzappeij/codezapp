<div>
	<div class="relative"> 
		<div class="transform -translate-y-2/4 -translate-x-2/4 absolute" style="top: 50%; left: 50%;">
			<select name="difficulty" id="difficulty" class="text-xl py-5 px-3">
				<option value="easy">Easy</option>
				<option selected value="normal">Normal</option>
				<option value="hard">Hard</option>
				<option value="impossible">Impossible</option>
			</select>
			<button id="startButton" class="py-5 px-10 bg-brand-yellow font-bold text-xl uppercase inline-block border-0">Start Game</button>
		</div>
    	<canvas id="SnakeGameCanvas" class="mx-auto" width="1000" height="500"></canvas>
	</div>

    <script>
        document.addEventListener('livewire:load', function () {	
			const board_border = '#636465';
		    const board_background = "#12120f";
		    const snake_col = '#636465';
		    const snake_border = '#12120f';

		    var snake = [
		      {x: 240, y: 200},
		      {x: 220, y: 200},
		      {x: 200, y: 200},
		      {x: 180, y: 200},
		      {x: 160, y: 200}
		    ];

		    var levels = {
		    	'easy': {
		    		'speed': 150,
		    		'win_score': 200 
		    	},
		    	'normal': {
		    		'speed': 100,
		    		'win_score': 250 
		    	},
		    	'hard': {
		    		'speed': 50,
		    		'win_score': 200 
		    	},
		    	'impossible': {
		    		'speed': 25,
		    		'win_score': 100 
		    	},
		    };
		    let level = 'normal';

		    let score = 0;
		    let win_score = 250;

		    let changing_direction = false;

		    let food_x;
		    let food_y;
		    let dx = 20;
		    let dy = 0;
    
		    const snakeboard = document.getElementById("SnakeGameCanvas");
		    const snakeboard_ctx = snakeboard.getContext("2d");

        	drawSnake();
		    gen_food();

		    document.addEventListener("keydown", change_direction);

		    function resetGame(){
		    	document.getElementById('startButton').classList.remove('hidden');
		    	document.getElementById('difficulty').classList.remove('hidden');
		    	snake = [
			      {x: 240, y: 200},
			      {x: 220, y: 200},
			      {x: 200, y: 200},
			      {x: 180, y: 200},
			      {x: 160, y: 200}
			    ];
			    score = 0;
	    		dx = 20;
	    		dy = 0;
	    		changing_direction = false;
				keyPressed = false;
		    }
		    
		    function main() {

			    window.addEventListener("keydown", function(e) {
				    if(["Space","ArrowUp","ArrowDown","ArrowLeft","ArrowRight"].indexOf(e.code) > -1) {
				        e.preventDefault();
				    }
				}, false);
				
		    	if (score >= levels[level]['win_score']) {
					snakeboard_ctx.fillStyle = "green";
					snakeboard_ctx.font = '40px Orbitron';

					var textString = "You win!!",
				    textWidth = snakeboard_ctx.measureText( textString ).width;

					snakeboard_ctx.fillText(textString , (snakeboard.width/2) - (textWidth / 2), 180);

					resetGame();
					return;
		    	} 

		        if (has_game_ended()) {
					snakeboard_ctx.fillStyle = "red";
					snakeboard_ctx.font = '40px Orbitron';

					var textString = "Game Over!!",
				    textWidth = snakeboard_ctx.measureText( textString ).width;

					snakeboard_ctx.fillText(textString , (snakeboard.width/2) - (textWidth / 2), 180);

					resetGame();
					return;
		        }

		        changing_direction = false;
		        setTimeout(function onTick() {
		        	clear_board();
		        	drawFood();
		        	move_snake();
		        	drawSnake();

		       		main(); // Repeat
		      	}, levels[level]['speed'])
		    }
		    
		    function clear_board() {
		      	snakeboard_ctx.fillStyle = board_background;
		      	snakeboard_ctx.strokeStyle = board_border;
		      	snakeboard_ctx.fillRect(0, 0, snakeboard.width, snakeboard.height);
		      	snakeboard_ctx.strokeRect(0, 0, snakeboard.width, snakeboard.height);
		    }
		    
		    function drawSnake() {
		      	snake.forEach(drawSnakePart)
		    }

		    function drawFood() {
		      	snakeboard_ctx.fillStyle = ' rgb(252, 186, 2)';
		      	snakeboard_ctx.strokeStyle = ' rgb(252, 186, 2)';
		      	snakeboard_ctx.fillRect(food_x, food_y, 20, 20);
		     	snakeboard_ctx.strokeRect(food_x, food_y, 20, 20);
		    }
		    

		    function drawSnakePart(snakePart) {
		      	snakeboard_ctx.fillStyle = snake_col;
		      	snakeboard_ctx.strokeStyle = snake_border;
		      	snakeboard_ctx.fillRect(snakePart.x, snakePart.y, 20, 20);
		      	snakeboard_ctx.strokeRect(snakePart.x, snakePart.y, 20, 20);
		    }

		    function has_game_ended() {
		      	if(score > win_score ){
		      		return true;
		      	}
		      	for (let i = 4; i < snake.length; i++) {
		        	if (snake[i].x === snake[0].x && snake[i].y === snake[0].y) return true
		     	}

		      	const hitLeftWall = snake[0].x < 0;
		      	const hitRightWall = snake[0].x > snakeboard.width - 20;
		      	const hitToptWall = snake[0].y < 0;
		      	const hitBottomWall = snake[0].y > snakeboard.height - 20;
		      	return hitLeftWall || hitRightWall || hitToptWall || hitBottomWall
		    }

		    function random_food(min, max) {
		      	return Math.round((Math.random() * (max-min) + min) / 20) * 20;
		    }

		    function gen_food() {
		      	food_x = random_food(0, snakeboard.width - 20);
		      	food_y = random_food(0, snakeboard.height - 20);

		      	snake.forEach(function has_snake_eaten_food(part) {
	        		const has_eaten = part.x == food_x && part.y == food_y;
		        	if (has_eaten) {
		        		gen_food();
		       	 	}
		     	});
		    }

		    function change_direction(event) {
				const LEFT_KEY = 37;
				const RIGHT_KEY = 39;
				const UP_KEY = 38;
				const DOWN_KEY = 40;
		    
		      	if (changing_direction) return;
		      	changing_direction = true;
		      	const keyPressed = event.keyCode;
		      	const goingUp = dy === -20;
		      	const goingDown = dy === 20;
		      	const goingRight = dx === 20;
		      	const goingLeft = dx === -20;

		      	if (keyPressed === LEFT_KEY && !goingRight) {
		        	dx = -20;
		        	dy = 0;
		      	}
		      	if (keyPressed === UP_KEY && !goingDown) {
		        	dx = 0;
		        	dy = -20;
		      	}
		      	if (keyPressed === RIGHT_KEY && !goingLeft) {
		        	dx = 20;
		        	dy = 0;
		      	}
		      	if (keyPressed === DOWN_KEY && !goingUp) {
		        	dx = 0;
		        	dy = 20;
		      	}
		    }

		    function move_snake() {
		      	const head = {x: snake[0].x + dx, y: snake[0].y + dy};
		      	snake.unshift(head);
		      	const has_eaten_food = snake[0].x === food_x && snake[0].y === food_y;
		     	if (has_eaten_food) {
			        score += 10;
			        document.getElementById('score').innerHTML = score + '/' + levels[level]['win_score'];
		        	levels[level]['speed'] = levels[level]['speed'] * 0.95;
			        if(score < win_score) {
			        	gen_food();
			        }
		      	} else {
		        	snake.pop();
		      	}
		    }

		    startButton.addEventListener("click", function(e) {
	        	clear_board();
	        	drawSnake();

		    	this.classList.add('hidden');
		    	level = document.getElementById('difficulty').value;
		    	document.getElementById('difficulty').classList.add('hidden');
		        document.getElementById('score').innerHTML = score + '/' + levels[level]['win_score'];
		    	main();
		    });
		});
    </script>	
</div>
