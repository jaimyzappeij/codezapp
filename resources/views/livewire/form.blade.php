<div>
	<form class="dark-form" wire:submit.prevent="submitForm">
		@foreach($fields as $field)
			<div class="text-white mt-3">
				<label class="text-xl leading-10">
					{{ $field['label'] }}
				</label>
				@switch($field['type'])
					@case('text')
					@case('email')
					@case('phone')
					@case('number')
						<input  wire:model="{{ $field['name'] }}" type="{{ $field['type'] }}" name="{{ $field['name'] }}" placeholder="{{ $field['placeholder'] }}">
						@error($field['name']) <span class="error">{{ $message }}</span> @enderror
						@break
					@case('textarea')
						<textarea  wire:model="{{ $field['name'] }}" rows="5" class="leading-6" name="{{ $field['name'] }}"></textarea> 
						@error($field['name']) <span class="error">{{ $message }}</span> @enderror
						@break
				@endswitch	
			</div>
		@endforeach
		@if(!$submitted)
			<div class="w-1/2 md:w-3/12 mt-6"><input type="submit" value="Send"></div>
		@else
			<span class="success text-white">The form has successfully been submitted. You will be contacted shortly!</span> 
		@endif
	</form>
</div>
