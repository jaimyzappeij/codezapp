<div class="browser" id="browser">
	<div class="head">
	    <div class="actions">
	        <span class="close action"></span>
	        <span class="small action"></span>
	        <span class="full action"></span>
	    </div>
	    <div class="tabs">
	    	@foreach($tabs as $tab)
		        <div class="tab {{ $tab->slug == $active ? 'active' : '' }}" data-page="{{ $tab->slug }}"  wire:click="setActivePage('{{ $tab->slug }}')">
		        	{{ $tab->title }}
		        </div>
        	@endforeach	
	    </div>
	</div>
	<div class="search-bar">
	    <input type="text" value="https://codezapp.com/{{ $active }}" wire:keydown.enter="newBrowserTab($event.target.value.split('.com/')[1])">
	</div>
	<div class="body overflow-scroll">
	    @foreach($pages as $page)
	       	<div class="page {{ $page->slug == $active ? 'active' : '' }}" data-page="{{ $page->slug }}">
	       		{!! Templater::render($page->content) !!}
	    	</div>
    	@endforeach	
	</div>
</div>