<div class="flex stack-logos flex-row flex-wrap mt-16 md:mt-0">
	@foreach($logos as $logo)
	    <div class="w-3/12 md:w-6/12 lg:w-4/12  xl:w-4/12 2xl:w-3/12 box-border {{ $logo->class }}" wire:click="addNewTab('{{ $logo->slug }}')">
	        <img data-title="{{ $logo->title }}" src="{{ $logo->image }}" alt="{{ $logo->title }} logo" class="filter grayscale" data-page="{{ $logo->slug }}">
	    </div>
    @endforeach	 
</div>