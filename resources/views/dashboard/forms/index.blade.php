@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Forms > index</h1>
                <p class="text-white">
                   On this page you can add new forms which can be used in the website as a livewire component.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Add new</div>
                        <div class="p-4 text-white">
                            <form action="#" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @csrf
                                <div class="flex gap-3 w-full">
                                    <div class="w-5/12">
                                        <input type="text" name="title" placeholder="Title" class="">
                                    </div>  
                                    <div class="w-4/12">
                                        <input type="text" name="slug" placeholder="Slug">
                                    </div> 
                                    <div class="w-3/12">
                                        <input type="submit" value="Add Form" class="self-end">
                                    </div> 
                                </div>   
                            </form>
                        </div>
                    </div>

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Forms</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3">
                            @foreach($forms as $form)    
                                <form action="{{ url('/dashboard/forms/'.$form->id) }}" method="post" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                     @csrf
                                    <div class="flex gap-3 w-full">
                                    <div class="w-5/12">
                                        <input type="text" disabled="" name="title" value="{{ $form->title }}" placeholder="Title" class="">
                                    </div>  
                                    <div class="w-4/12">
                                        <input type="text" disabled="" name="slug" value="{{ $form->slug }}" placeholder="Slug">
                                    </div> 
                                    <div class="w-3/12">
                                        <input type="submit" value="Edit form" class="self-end">
                                    </div> 
                                </div>
                                </form>

                                <hr class="border-0 h-0.5 bg-browser-border w-full my-3">
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
