@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Forms > {{ $form->title }}</h1>
                <p class="text-white">
                   On this page you can add new forms which can be used in the website as a livewire component.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Shortcode</div>
                        <div class="p-4 text-white">
                            <form class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @php( $short = "@livewire('form', ['slug' => '".$form->slug."'])" )
                                <input type="text" disabled="" value="{{ $short }}">
                            </form> 
                        </div>
                    </div>  

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Add new field</div>
                        <div class="p-4 text-white">
                            <form action="{{ url('dashboard/forms/update/'.$form->id) }}" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @csrf
                                <input type="hidden" name="action" value="add">
                                <div class="flex gap-3 w-full">
                                    <div class="w-5/12">
                                        <input type="text" name="name" placeholder="Name" class="">
                                        <input type="text" class="mt-3" name="label" placeholder="Label" class="">
                                    </div>  
                                    <div class="w-4/12">
                                        <input type="text" name="placeholder" class="mb-3" placeholder="Placeholder">
                                        <select name="type">
                                            <option value="text">Text</option>
                                            <option value="number">Number</option>
                                            <option value="phone">Phone</option>
                                            <option value="email">Email</option>
                                            <option value="textarea">Textarea</option>
                                        </select>
                                    </div> 
                                    <div class="w-3/12">
                                        <input type="checkbox" id="required" name="required">
                                        <label for="required">Field is required</label>
                                        <input type="submit" value="Add field" class="self-end mt-3">
                                    </div> 
                                </div>   
                            </form>
                        </div>
                    </div>

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Form fields</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3" wire:ignore>
                            <form action="{{ url('/dashboard/forms/update/'.$form->id) }}" method="post" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @csrf
                                @foreach($fields as $key => $field)    
                                    <div class="flex gap-3 w-full"  x-ref="field-{{ $key }}" x-data="
                                        { 
                                            fieldName: '{{ $field->name }}',
                                            getFieldName(field) { return field+'---'+this.fieldName; }
                                        }
                                    ">
                                        <div class="w-5/12">
                                            <input type="text" :name="getFieldName('name')" placeholder="Name" value="{{ $field->name }}"
                                                    x-on:change="fieldName = $event.target.value" >
                                            <input type="text" class="mt-3" :name="getFieldName('label')" placeholder="Label" value="{{ $field->label }}">
                                        </div>  
                                        <div class="w-4/12">
                                            <input type="text" :name="getFieldName('placeholder')" class="mb-3" placeholder="Placeholder" value="{{ $field->placeholder }}">
                                            <select :name="getFieldName('type')">
                                                <option value="text" {{ $field->type === 'text' ? 'selected' : '' }}>Text</option>
                                                <option value="number" {{ $field->type === 'number' ? 'selected' : '' }}>Number</option>
                                                <option value="phone" {{ $field->type === 'phone' ? 'selected' : '' }}>Phone</option>
                                                <option value="email" {{ $field->type === 'email' ? 'selected' : '' }}>Email</option>
                                                <option value="textarea" {{ $field->type === 'textarea' ? 'selected' : '' }}>Textarea</option>
                                            </select>
                                        </div> 
                                        <div class="w-3/12">
                                            <input type="checkbox" :id="getFieldName('required')" :name="getFieldName('required')" {{ $field->required === 1 ? 'checked' : '' }}>
                                            <label :for="getFieldName('required')">Field is required</label>
                                            <div x-on:click="$refs['field-{{ $key }}'].remove()" class="rounded-md py-3 mt-3 px-10 bg-brand-error text-center">Remove Field</div>
                                        </div> 

                                    </div>   
                                    <hr class="border-0 h-0.5 bg-browser-border w-full my-3">
                                @endforeach
                                <input type="submit" value="Update fields">
                            </form> 
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
