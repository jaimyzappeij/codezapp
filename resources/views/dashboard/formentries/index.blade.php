@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Home > Form entries</h1>
                <p class="text-white">
                   On this page you can view the form entries.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Form Entries</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3">
                            @foreach($entries as $entry)    
                                <form class="dark-form flex gap-3 flex-wrap flex-row w-full" x-data="{ open: false }">
                                     @csrf
                                    <div class="flex gap-3 w-full">
                                        <div class="w-3/12">
                                            <input type="text" disabled name="form" value="{{ $entry->form->title }}">
                                        </div>  
                                        <div class="w-5/12">
                                            <input type="text" disabled name="email" value="{{ $entry->fields['email'] }}">
                                        </div> 
                                        <div class="w-3/12">
                                            <input type="text" disabled name="date" value="{{ $entry->created_at }}">
                                        </div> 
                                        <div class="w-1/12">
                                            <div x-on:click="open = ! open" class="py-3 w-full block rounded-md text-center bg-brand-yellow">View</div> 
                                        </div> 
                                    </div>   

                                    <div x-show="open" class="fixed w-10/12 max-h-10/12 bg-browser-light transform -translate-y-1/2 -translate-x-1/2 border border-browser-border scroll" style="left: 50%; top: 50%">
                                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">
                                            <div class="float-left text-3xl">{{ $entry->form->title }} | Entry: {{ $entry->created_at }}</div>
                                            <div class="float-right bg-brand-error py-2 px-4 text-md" x-on:click="open = ! open">Close</div>
                                            <div class="clear-both"></div>
                                        </div>
                                        <div class="p-4 text-white h-screen">
                                            <div>
                                                @foreach($entry->fields as $field => $value)
                                                    <label class="text-xl font-bold capitalize">{{ $field }}</label>
                                                    <div class="w-full bg-browser-dark text-white rounded-md py-4 mb-3 text-xl px-4">{{ $value }}</div>
                                                @endforeach
                                            </div>  
                                        </div>
                                    </div>  
                                </form>

                                <hr class="border-0 h-0.5 bg-browser-border w-full my-3">
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
