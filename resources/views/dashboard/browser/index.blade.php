@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Home > Browser pages</h1>
                <p class="text-white">
                   On this page you can change the browser pages shown in the homepage browser section.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Add new</div>
                        <div class="p-4 text-white">
                            <form action="#" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @csrf
                                <div class="flex gap-3 w-full">
                                    <div class="w-11/12">
                                        <div class="flex gap-3"> 
                                            <div class="w-5/12">
                                                <input type="text" name="title" placeholder="Title" class="">
                                            </div>  
                                            <div class="w-4/12">
                                                <input type="text" name="slug" placeholder="Slug">
                                            </div> 
                                            <div class="w-3/12">
                                                <input type="checkbox" name="opened" id="opened">
                                                <label for="opened">Make page opened by default</label>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="w-1/12">
                                    </div>  
                                </div>   
                                <div class="flex gap-3 w-full">
                                    <div class="w-11/12">
                                        <textarea name="content" row="3" class="leading-6 py-4 h-32"></textarea> 
                                    </div>  
                                    <div class="w-1/12 flex">
                                        <input type="submit" value="Add label" class="self-end">
                                    </div>  
                                </div>  
                            </form>
                        </div>
                    </div>

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Text labels</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3">
                            @foreach($pages as $page)    
                                <form action="{{ url('dashboard/home/browserpages/'.$page->id) }}" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                     @csrf
                                    <div class="flex gap-3 w-full">
                                        <div class="w-11/12">
                                            <div class="flex gap-3"> 
                                                 <div class="w-5/12">
                                                <input type="text" name="title" placeholder="Title" class="" value="{{ $page->title }}">
                                                </div>  
                                                <div class="w-4/12">
                                                    <input type="text" name="slug" placeholder="Slug" value="{{ $page->slug }}">
                                                </div> 
                                                <div class="w-3/12">
                                                    <input type="checkbox" name="opened" id="opened-{{ $page->id }}" {{ ($page->opened ? 'checked' : '') }}>
                                                    <label for="opened-{{ $page->id }}">Make page opened by default</label>
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="w-1/12">
                                        </div>  
                                    </div>   
                                    <div class="flex gap-3 w-full">
                                        <div class="w-11/12">
                                            <textarea name="content" row="3" class="leading-6 py-4 h-24">{{ $page->content }}</textarea> 
                                        </div>  
                                        <div class="w-1/12 flex">
                                            <input type="submit" value="Update" class="self-end">
                                        </div>  
                                    </div>  
                                </form>

                                <hr class="border-0 h-0.5 bg-browser-border w-full my-3">
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
