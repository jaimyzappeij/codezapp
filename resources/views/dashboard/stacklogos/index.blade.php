@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Home > Stack logos</h1>
                <p class="text-white">
                   On this page you can change the stack logos shown on your front page.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Add new</div>
                        <div class="p-4 text-white">
                            <form action="#" method="POST" class="dark-form">
                                @csrf
                                <div class="flex gap-3">
                                    <div class="w-6/12">
                                        <input type="text" name="image" placeholder="Image">
                                    </div>  
                                    <div class="w-2/12">
                                        <input type="text" name="slug" placeholder="Slug">
                                    </div>  
                                    <div class="w-2/12">
                                        <input type="text" name="title" placeholder="Title">
                                    </div>  
                                    <div class="w-1/12">
                                        <input type="text" name="class" placeholder="Class">
                                    </div>  
                                    <div class="w-1/12">
                                        <input type="submit" value="Add logo">
                                    </div>  
                                </div>  
                            </form>
                        </div>
                    </div>

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Stack logos</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3">
                            @foreach($stacklogos as $logo)    
                                <form action="{{ url('dashboard/home/stacklogos/'.$logo->id) }}" method="POST" class="dark-form w-full">
                                    @csrf
                                    <div class="flex gap-3">
                                        <div class="w-6/12">
                                            <input type="text" name="image" placeholder="Image" value="{{ $logo->image }}">
                                        </div>  
                                        <div class="w-2/12">
                                            <input type="text" name="slug" placeholder="Slug" value="{{ $logo->slug }}">
                                        </div>  
                                        <div class="w-2/12">
                                            <input type="text" name="title" placeholder="Title" value="{{ $logo->title }}">
                                        </div>  
                                        <div class="w-1/12">
                                            <input type="text" name="class" placeholder="Class" value="{{ $logo->class }}">
                                        </div>  
                                        <div class="w-1/12">
                                            <input type="submit" value="Update">
                                        </div>  
                                    </div>  
                                </form>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
