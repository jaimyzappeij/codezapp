@extends('layouts.app')

@section('content')
<main class="sm:container sm:mx-auto sm:mt-10">
    <div class="w-full">

        <section class="flex flex-col break-words">
            <div class="w-full p-6">
                <h1 class="text-white text-5xl font-bold mb-5">Home > Labels</h1>
                <p class="text-white">
                   On this page you can change the text label shown on website.
                </p>
                <div class="flex gap-2 mt-8 flex-row flex-wrap">

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm mb-6">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Add new</div>
                        <div class="p-4 text-white">
                            <form action="#" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                @csrf
                                <div class="flex gap-3 w-full">
                                    <div class="w-11/12">
                                        <div class="flex gap-3"> 
                                            <div class="w-1/2">
                                                <input type="text" name="key" placeholder="Key" class="">
                                            </div>  
                                            <div class="w-1/2">
                                                <input type="text" name="location" placeholder="Location (optional)">
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="w-1/12">
                                    </div>  
                                </div>   
                                <div class="flex gap-3 w-full">
                                    <div class="w-11/12">
                                        <textarea name="text" row="3" class="leading-6 py-4 h-32"></textarea> 
                                    </div>  
                                    <div class="w-1/12 flex">
                                        <input type="submit" value="Add label" class="self-end">
                                    </div>  
                                </div>  
                            </form>
                        </div>
                    </div>

                    <div class="w-full bg-browser-light border border border-browser-border rounded-sm">
                        <div class="bg-browser-dark text-white p-4 font-bold text-xl">Text labels</div>
                        <div class="p-4 text-white flex flex-row flex-wrap gap-3">
                            @foreach($labels as $label)    
                                <form action="{{ url('dashboard/home/labels/'.$label->id) }}" method="POST" class="dark-form flex gap-3 flex-wrap flex-row w-full">
                                     @csrf
                                    <div class="flex gap-3 w-full">
                                        <div class="w-11/12">
                                            <div class="flex gap-3"> 
                                                <div class="w-1/2">
                                                    <input type="text" name="key" placeholder="Key" value="{{ $label->key }}" class="">
                                                </div>  
                                                <div class="w-1/2">
                                                    <input type="text" name="location"  value="{{ $label->location }}" placeholder="Location (optional)">
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="w-1/12">
                                        </div>  
                                    </div>   
                                    <div class="flex gap-3 w-full">
                                        <div class="w-11/12">
                                            <textarea name="text" row="3" class="leading-6 py-4 h-24">{{ $label->text }}</textarea> 
                                            <div class="bg-browser-dark p-4 rounded-xl mt-3 text-xl">
                                                {!! $label->text !!}
                                            </div>
                                        </div>  
                                        <div class="w-1/12 flex">
                                            <input type="submit" value="Update" class="self-end">
                                        </div>  
                                    </div>  
                                </form>

                                <hr class="border-0 h-0.5 bg-browser-border w-full my-3">
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</main>
@endsection
