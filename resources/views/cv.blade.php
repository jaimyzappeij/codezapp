<html>
	<head>
		<title>CV | Jaimy Zappeij</title>
		<style>
			@page{
				margin: 0;
			}
			body{
				background: rgb(32,33,36);
				font-family: 'Open Sans', sans-serif;
				margin: 0;
			}
			h1{
				font-family: 'Montserrat', sans-serif;
				font-size: 40px;
				margin-bottom: -8px;
			}
			.page {
				background: rgb(32,33,36);
				padding-left: 15px;
			}
			.head .pf,
			.head .name {
				float: left;
				color:  #fff;
			}
			.head .name {
				margin-left: 10px;
				background: rgb(53,54,58);
				padding: 20px 22px 0;
				height: 165px;
				margin-top: 15px;
				width: 500px;
				box-sizing: border-box;
			}
			.clear{
				clear: both;
			}
			.tag{
				background: #002D8C;
				color: #fff;
				display:  inline-block;
				padding: 4px 12px;
			}
			.side {
				width: 202px; 
				float: left;
			}
			.content {
				width: 500px; 
				float: left;
				margin-left: 15px;
				padding-left: 15px;
				border-left: 2px solid rgb(53,54,58);
			}
			.body {
				padding: 30px 0;
			}
			.mt-0{
				margin-top: 0;
			}
			.mt-1{
				margin-top: 1em;
			}
			.mb-1{
				margin-bottom: 1em;
			}
			p, ul, li{
				font-size: 14px; 
				line-height: 1.5em;
				color:  #fff;
			}
			.progress-bar {
				margin-top: -8px;
				height: 8px;
				background: rgb(53,54,58);
				position: relative;
				margin-bottom: 10px;
			}
			.progress-bar div{
				height: 8px;
				position: absolute;
				left:  0;
				top:  0;
				background: #fff;
			}
			.date {
				color:  rgb(124,129,134);
				font-size: 11px;
			}
			ul {
				margin: 0;
				padding-left: 18px;
			}
			hr{
				border: none;
				background: rgb(53,54,58);
				height: 2px;
			}
		</style>
	</head>
	<body>
		<div class="page">
			<div class="head">
				<div class="pf">
					<img src="{{ public_path().'/img/pf.png' }}" alt="" width="200px">
				</div>
				<div class="name">
					<h1>Jaimy Zappeij</h1>
					<h5 class="tag">PHP developer</h5>
				</div>
				<div class="clear"></div>
			</div>
			<div class="body">
				<div class="side">
					<h5 class="tag mt-0 mb-1">Contact</h5>
					<p class="mt-0">
						jzappeij.jz@gmail.com<br>
						06 57061305 
					</p>

					<h5 class="tag mt-0 mb-1">Skills</h5>
					<p class="mt-0">PHP <span style="float: right;">5+ jaar</p>
					<div class="progress-bar"><div style="width: 90%"></div></div>
					<p class="mt-0">Laravel <span style="float: right;">4+ jaar</span></p>
					<div class="progress-bar"><div style="width: 90%"></div></div>
					<p class="mt-0">Git <span style="float: right;">3+ jaar</span></p>
					<div class="progress-bar"><div style="width: 85%"></div></div>
					<p class="mt-0">SQL <span style="float: right;">5+ jaar</span></p>
					<div class="progress-bar"><div style="width: 77%"></div></div>
					<p class="mt-0">JavaScript <span style="float: right;">5+ jaar</span></p>
					<div class="progress-bar"><div style="width: 68%"></div></div>
					<p class="mt-0">React (Native) <span style="float: right;">2+ jaar</span></p>
					<div class="progress-bar"><div style="width: 60%"></div></div>
					<p class="mt-0">HTML + CSS <span style="float: right;">5+ jaar</span></p>
					<div class="progress-bar"><div style="width: 85%"></div></div>
					<p class="mt-0">Linux <span style="float: right;">3+ jaar</span></p>
					<div class="progress-bar"><div style="width: 50%"></div></div>
				</div>
				<div class="content">
					<h5 class="tag mt-0 mb-1">Profiel</h5>
					<p class="mt-0">PHP developer met 5+ jaar ervaring met het bouwen van web applicaties met verschillende technieken. Als developer vind ik het belangrijk om nooit te stoppen met leren, daarom omring ik mezelf graag met andere developers die mij naar een volgend niveau kunnen brengen. Natuurlijk houd ik er ook van om mijn kennis te delen met anderen om samen de hoogst mogelijke kwaliteit te leveren.</p>
					<div class="margin-bottom:10px;"></div>
					<h5 class="tag mb-1 mt-0">Ervaring</h5>
					<p class="mt-0">
						<strong>Back-end developer, To The Point Digital, Leiderdorp</strong><br>
						<span class="date">JAN 2021  - HEDEN</span>
						<p style="margin: -8px 0 0;">
							Mijn werkzaamheden hier bevatten:
							<ul>
								<li>Het bouwen van maatwerk koppelingen met 3e partijen</li>
								<li>Het bouwen van maatwerk applicaties op basis van WordPress</li>
								<li>Het aansturen van offshore developers</li>
							</ul>
						</p>	
					</p>
					<hr class="mb-1 mt-1">
					<p class="mt-0">
						<strong>Back-end developer, Buro N11, Bodegraven</strong><br>
						<span class="date">MEI 2016  - DEC 2020</span>
						<p style="margin: -8px 0 0;">
							Mijn werkzaamheden hier waren:
							<ul>
								<li>Het bouwen van custom Wordpress plugins en thema's</li>
								<li>Het bouwen van webshops met woocommerce</li>
								<li>Het bouwen van websites en systemen op basis van Laravel</li>
								<li>Het automatiseren van taken met Shell</li>
							</ul>
						</p>	
					</p>
					<h5 class="tag mt-1">Opleidingen / Cursussen</h5>
					<p class="mt-1">
						<strong>Mediadeveloper BBL, MBO Rijnland</strong><br>
						<span class="date">SEP 2015 - JUN 2018</span>
					</p>
					<p class="mt-0">
						<strong>Linux professional HBO <span class="date">(Niet afgerond)</span></strong><br>
						<span class="date">2019 - 2019</span>
					</p>
					<p class="mt-0">
						<strong>React Native, Eduvision</strong><br>
						<span class="date">APR 2019 - APR 2019</span>
					</p>


						
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</body>
</html>